![pandora logo](./assets/logo.png)

# Pandora

O projeto Pandora é uma caixa automatizada para recebimento e armazenamento de encomendas em residências, visando aprimorar a conveniência e a praticidade da logística em compras online. Com o avanço do comércio digital, torna-se crucial avaliar os impactos e as novas demandas dos consumidores nesse contexto. O desafio abordado é a necessidade de disponibilidade para receber as entregas ou retirá-las em agências, afetando a experiência do usuário em marketplaces.

O sistema consiste em uma caixa de entregas Pandora e um aplicativo de acompanhamento Pandora. O sistema emite um comprovante ao entregador e notifica o proprietário da caixa sobre a recepção da encomenda. O acesso e retirada das encomendas são realizados pelo usuário através de uma porta exclusiva, proporcionando uma gestão eficiente do recebimento de encomendas.

### Rodando o Projeto

1. Instale o **docsify** em sua maquina com o comando a seguir:

    `npm i docsify-cli -g`

2.  De volta ao terminal, execute o seguinte comando para iniciar o projeto:

    `docsify serve docs`

## Contribuidores

|![Sofia Patrocinio](./assets/sofia.png){ width="60" height="60"}|![Irwin Schmitt](./assets/irwin.png){ width="60" height="60"}|![Leonardo da Silva Gomes](./assets/leo.png){ width="60" height="60"}|![Ian Rocha](./assets/ian.png){ width="60" height="60"}|![Thais Araujo](./assets/thais.png){ width="60" height="60"}|![Carlos Fiuza](./assets/carlos.png){ width="60" height="60"}|![Herick](./assets/herick.png){ width="60" height="60"}|![Serafim](./assets/serafim.png){ width="60" height="60"}|
|---|---|---|---|---|---|---|---|
|Sofia Patrocinio|Irwin Schmitt|Leonardo Gomes|Ian Rocha|Thais Araujo|Carlos Fiuza|Herick Portugues|Phelipe Serafim|

|![Nei Lopes](./assets/nei.png){ width="60" height="60"}|![Ivana Figueiredo](./assets/ivana.png){ width="60" height="60"}|![Arthur Nogueira](./assets/arthur.png){ width="60" height="60"}|![Gabriela Reinert](){ width="60" height="60"}|
|---|---|---|---|
|Nei Lopes|Ivana Figueiredo|Arthur Nogueira|Gabriela Reinert|

|![Yuri Cesar](./assets/yuri.png){ width="60" height="60"}|![Henrique Moura](./assets/henrique.png){ width="60" height="60"}|
|---|---|
|Yuri Cesar|Henrique Moura|

