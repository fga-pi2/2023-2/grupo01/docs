# Arquitetura Geral da Solução

A arquitetura geral do produto desenvolvido pode ser vista como a junção das
arquiteturas das áreas de Eletrônica, Estrutura, Energia e Software. A parte estrutural
do produto irá conter os sensores eletrônicos. Esses sensores irão enviar informações para
um servidor, onde os dados serão analisados e disponibilizados para os usuários por meio
de um aplicativo mobile.

O detalhamento das arquiteturas citadas pode ser visto nos seguintes documentos:

- [Software](software/arquitetura.md)
- [Eletrônica]()
- [Estrutura]()