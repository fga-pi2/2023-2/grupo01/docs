# Requisitos Gerais

## Elicitação

Para a elicitação dos requisitos, utilizamos a técnica de *brainstorming*, cujo objetivo principal é explorar as ideias e interagir com os membros da equipe, onde todos podem compartilhar suas opiniões.
Essa técnica é utilizada normalmente no inicio do processo de elicitação, quando muito pouco do projeto e do processo é conhecido.

Inicialmente o brainstorming foi feito com toda a equipe da Pandora, e depois foi refinado pelas equipes de software, eletrônica e estrutura gerando requisitos específicos de cada time.

## Requisitos Funcionais Gerais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RFG01 | Controle de Acesso | O sistema deve permitir a abertura da caixa apenas por usuários autorizados através de autenticação. |
| RFG02 | Funcionamento autônomo | O sistema deve ser alimentado por uma fonte de energia independente da tomada, como bateria ou painel solar. |
| RFG03 | Notificação ao Usuário | O sistema deve enviar uma notificação ao usuário quando sua encomenda for entregue com sucesso, incluindo detalhes como data e hora da entrega. |
| RFG04 | Segurança e resistência |  O sistema deve ser construído com materiais resistentes e duráveis para proteger as encomendas contra condições climáticas adversas. |
| RFG05 | Registro de Acesso | O sistema deve registrar as interações com a caixa, incluindo tentativas de abertura e horários. |
| RFG06 | Notificação de Alerta | O sistema deve enviar uma notificação ao usuário quando houver tentativa negada de abertura da caixa. |
| RFG07 | Armazenamento múltiplo | O sistema deve ser capaz de armazenar inúmeros pacotes de forma segura, garantindo que o entregador não tenha acesso a outros pacotes. |
| RFG08 | Backup de dados | Deve haver um sistema de backup para garantir que as informações das entregas não sejam perdidas em caso de falha do sistema principal. |
| RFG09 | Peso máximo | O sistema deve ser capaz de suportar e informar o peso máximo de 5Kg por pacote. |
| RFG10 | Dimensões máximas | O sistema deve ser capaz de suportar e informar as dimensões máximas de 150x150x150 mm por pacote. |
| RFG11 | Proteção de Dados | O sistema deve cumprir as regulamentações de segurança e proteção de dados. |

## Requisitos Não Funcionais Gerais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RNFG01 | Usabilidade | A interface de usuário da caixa receptora deve ser intuitiva e de fácil utilização para os destinatários e entregadores. |
| RNFG02 | Disponibilidade |  O sistema deve estar disponível para uso em qualquer horário e dia da semana. |
| RNFG03 | Confiabilidade | O sistema deve ser confiável, minimizando falhas e erros de funcionamento. |
| RNFG04 | Segurança na entrega | O sistema deve garantir a segurança das encomendas durante o processo de entrega. |
| RNFG05 | Privacidade | O sistema deve garantir a privacidade dos destinatários, impedindo que informações pessoais sejam acessadas por pessoas não autorizadas. |
| RNFG06 | Escalabilidade | O sistema deve ser dimensionado para acomodar um número crescente de usuários e pacotes conforme necessário. |
| RNFG07 | Interoperabilidade | O sistema deve ser compatível com sistemas de entrega e logística existentes, facilitando a integração com serviços de correio. |
| RNFG08 | Manutenção | O sistema deve ser de fácil manutenção e atualização de software e hardware. |
| RNFG09 | Consumo de Energia | O sistema deve ser eficiente em termos de consumo de energia para minimizar custos operacionais e impacto ambiental. |
| RNFG10 | Ergonomia | A altura e o design da caixa de entrega devem ser ergonomicamente adequados para facilitar a interação do usuário ao depositar ou retirar encomendas. |