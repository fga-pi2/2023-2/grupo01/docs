<!-- docs/_sidebar.md -->

- [**Home**]()

- **Geral**
    - [Requisitos](geral/requisitos.md)
    - [Arquitetura](geral/arquitetura.md)

- **Software**
    - **Requisitos**
        - [Elicitação](software/requisitos.md)
        - [backlog](software/backlog.md)
        - [Personas](software/personas.md)
    - [Arquitetura](geral/arquitetura.md)
    

- [**Eletrônica**](#)
    - [Requisitos](eletronica/requisitos.md)
    - [Arquitetura](geral/arquitetura.md)

- [**Estrutura**](#)
    - [Requisitos](estrutura/requisitos.md)
    - [Arquitetura](geral/arquitetura.md)
