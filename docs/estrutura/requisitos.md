# Requisitos de Estrutura

## Requisitos Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RFES01 | Porta para entregas | A caixa deverá ter uma porta com fechadura eletrônica que abrirá, a partir da inserção do código, dando acesso a um compartimento onde o pacote deverá ser entregue. |
| RFES02 | Ergonomia | A caixa deve ter uma altura tal que facilite o fornecimento de dados e a entrega feita pelo entregador. |
| RFES03 | Mecanismo de movimentação dos pacotes | A caixa possuirá um mecanismo de movimentação do pacote entregue, constituído por um atuador linear, que ao ser acionado pelo motor, empurrará o pacote para o interior da caixa. |
| RFES04 | Amortecimento da queda dos pacotes | Os pacotes entregues e empurrados para dentro da caixa deverão deslizar por uma rampa e cair no fundo da caixa com o amortecimento de espumas, para evitar que sejam danificados. |
| RFES05 | Resistência | A caixa deverá ser feita de material resistente, a fim de evitar sua violação. |
| RFES06 | Durabilidade | A caixa deve ser feita de material durável, pois ficará ao ar livre, sujeita a chuvas e incidência do Sol, que podem dinificar a estrutura.
. |
| RFES07 | Acesso do usuário aos pacotes armazenados | Haverá uma porta secundária, na parte traseira da caixa, com trava automática, pela qual o usuário poderá acessar seus pacotes entregues. |
| RFES08 | Fixação ao chão | A caixa deve ser chumbada ao chão, para evitar que seja roubada por inteiro. |

## Requisitos Não Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RNFES01 | Segurança dos pacotes | Os pacotes entregues devem estar seguros dentro da caixa, até que o usuário os recolha. Evitando qualquer tipo de dano ou roubo. |
| RNFES02 | Capacidade | A caixa deve conseguir armazenar uma certa quantidade de pacotes sem ser  preciso tirar as encomendas já entregues. |
| RNFES03 | Segurança interna | O entregador não conseguirá alcançar os pacotes armazenados anteriormente e nem os componentes eletrônicos contidos na caixa, mesmo com a porta aberta para o recebimento de encomendas. |
| RNFES04 | Manutenção | A caixa deve contar 2 portas de acesso, com fechadura manual, para manutenção de equipamentos internos. |