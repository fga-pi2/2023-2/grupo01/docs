# Requisitos de Eletrônica

## Requisitos Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RFEL01 | Motor Forte | Dedicado a funcionamento do mecanismo que transportará o pacote até a área de armazenamento. |
| RFEL02 | Fechaduras eletrônicas | Dedicado a funcionamento do travamento das portas |
| RFEL03 | Sensores de proximidade | Dedicado a verificação do fechamento das portas . |

## Requisitos Não Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RNFEL01 | Alimentação 12V | Dedicado a funcionamento do atuador. |
| RNFEL02 | Alimentação 5V | Dedicado a funcionamento dos microcontroladores e sensores.|
| RNFEL03 | Ponte H | Dedicado a inversão do sentido dos motores. |
| RNFEL04 | Corrente entre 5 ~ 8 A | Para funcionar o sistema dos motores. |