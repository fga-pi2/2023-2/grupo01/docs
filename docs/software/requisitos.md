# Requisitos de Software

## Requisitos Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RFSO01 | Criação de Conta | O usuário deve ser capaz de criar uma conta. |
| RFSO02 | Edição de Perfil | O usuário deve ser capaz de alterar informações da sua conta, incluindo nome, senha e outras informações relevantes. |
| RFSO03 | Visualização do Perfil | O usuário deve ser capaz de visualizar seu perfil, |
| RFSO04 | Exclusão de Conta | O usuário deve ser capaz de excluir sua conta. |
| RFSO05 | Login | O usuário deve ser capaz de fazer login. |
| RFSO06 | Logout | O usuário deve ser capaz de fazer logout. |
| RFSO07 | Cadastro de Encomenda | O usuário deve ser capaz de cadastrar uma encomenda. |
| RFSO08 | Criação de Categorias para Encomendas | O usuário deve ser capaz de criar categorias para organizar suas encomendas. |
| RFSO09 | Cancelamento de Encomenda | O usuário deve ser capaz de cancelar uma encomenda previamente cadastrada no sistema. |
| RFSO10 | Visualização de Detalhes da Encomenda | O usuário deve ser capaz de visualizar todos os detalhes de uma encomenda específica. |
| RFSO11 | Visualização de Encomendas por Categoria | O usuário deve ser capaz de visualizar suas encomendas organizadas por categorias que ele tenha criado. |
| RFSO12 | Visualização de Histórico de Encomendas | O usuário deve ser capaz de visualizar seu histórico de encomendas. |
| RFSO13 | Confirmação de Recebimento da Encomenda | O usuário entregador deve receber uma confirmação após o recebimento de uma encomenda. |
| RFSO14 | Visualização de Encomenda Negada | O usuário entregador deve visualizar caso a encomenda seja negada. |
| RFSO15 | Visualização de Lotação da Caixa | O usuário entregador deve ser capaz de visualizar que a caixa está cheia. |
| RFSO16 | Notificação de Entrega | O usuário deve poder receber uma notificação quando a encomenda for entregue. |
| RFSO17 | Notificação de Entrega Negada | O usuário deve poder receber uma notificação quando a encomenda for negada. |
| RFSO18 | Controle de Notificações | O usuário deve ser capaz de controlar quais notificações deseja receber. |
| RFSO19 | Notificação Caixa Cheia | O usuário deve poder receber uma notificação quando a caixa estiver cheia. |
| RFSO20 | Abertura da Porta | A caixa deve ser capaz de autorizar abertura da porta. |
| RFSO21 | Negativa de Abertura Caixa Cheia | A caixa deve ser capaz de negar abertura da porta caso esteja cheia |
| RFSO22 | Negativa de Abertura Motor Linear | A caixa deve ser capaz de negar abertura da porta caso o motor linear esteja atuando. |
| RFSO23 | Confirmação de Recebimento | A caixa deve ser capaz de confirmar o recebimento de uma encomenda. |

## Requisitos Não Funcionais

| ID   | Requisito | Descrição |
| ---- |-----------| ------------------------------------------------------------------------ |
| RNFSO01 | Disponibilidade do Serviço | O sistema deve estar disponível 24 horas por dia, 7 dias por semana, com disponibilidade alta para garantir que os usuários possam acessar o serviço sempre que necessário. |
| RNFSO02 | Tempo de Resposta | O sistema deve garantir tempo de resposta rápida para abertura da porta, para que o entregador possa realizar a entrega com sucesso. |
| RNFSO03 | Escalabilidade | O serviço deve ser escalável para lidar com um grande número de usuários, encomendas e entregadores. |
| RNFSO04 | Privacidade | O sistema deve cumprir regulamentações de privacidade de dados para proteger as informações pessoais dos usuários. |
| RNFSO05 | Tempo de Notificação | O sistema deve garantir que as notificações sejam entregues aos  usuários dentro de um tempo máximo aceitável, evitando atrasos significativos de comunicação. |
| RNFSO06 | Confiabilidade | O sistema de entrega deve ser confiável para garantir que as encomendas sejam entregues de maneira segura.|
| RNFSO07 | Compatibilidade | O sistema deve ser compatível com diferentes sistemas operacionais para garantir que os usuários possam acessar o serviço em diferentes ambientes.|
| RNFSO08 | Política de Privacidade e Termos de Uso | Os usuários devem ser claramente informados sobre as políticas de privacidade e termos de uso do sistema.|