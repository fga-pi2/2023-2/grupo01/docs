# Backlog

Nesta página é apresentado o Backlog criado para alinhamento das necessidades dos usuários definidos como público alvo. Além disso, cada História de Usuário foi priorizada utilizando a técnica *MoSCoW*, que é usada tanto para gestão, análise de negócios, quanto para o desenvolvimento de software e gerenciamento de projetos. Ele faz a categorização das histórias de usuários com os seguintes parâmetros:

- **_Must have_**: compreende os requisitos que são indispensáveis, e possui prioridade máxima no desenvolvimento.
- **_Shoul have_**: compreende os requisitos que são importantes, porém não são vitais do ponto de vista estratégico para o produto final.
- **_Could have_**: compreende os requisitos desejáveis, mas não essenciais do ponto de vista da entrega e satisfação do cliente.
- **_Wouldn't have_**: compreende os requisitos menos críticos, com menor retorno do investimento do produto final, podendo ser realizados posteriormente.

## Épicos

| ID | Épico |
| -- | ----- |
| E01 | Usuário |
| E02 | Controle |
| E03 | Caixa |

## Features

| ID | Feature |
| -- | ----- |
| F01 | Autenticação |
| F02 | Gerenciamento |
| F03 | Visualização |
| F04 | Notificação |
| F05 | Sistema Embarcado |


## E01 - Usuário
### F01 - Autenticação

| História de Usuário | Priorização | Descrição |
| -------- | ------ | ----------------------------------- |
| US01 | *Must* | Eu, como usuário, desejo ser capaz de criar uma conta |
| US02 | *Must* | Eu, como usuário, desejo ser capaz de alterar dados da minha conta |
| US03 | *Must* | Eu, como usuário, desejo ser capaz de visualizar meu perfil e as informações relativas a ele |
| US04 | *Must* | Eu, como usuário, desejo ser capaz de excluir minha conta |
| US05 | *Must* | Eu, como usuário, desejo ser capaz de fazer login na minha conta |
| US06 | *Must* | Eu, como usuário, desejo ser capaz de fazer logout na minha conta |

## E02 - Controle
### F02 - Gerenciamento

| História de Usuário | Priorização | Descrição |
| -------- | ------ | ----------------------------------- |
| US07 | *Must* | Eu, como usuário, desejo ser capaz de cadastrar uma encomenda |
| US08 | *Could* | Eu, como usuário, desejo ser capaz de criar categorias para organizar minhas encomendas |
| US09 | *Should* | Eu, como usuário, desejo ser capaz de cancelar uma encomenda |

### F03 - Visualização

| História de Usuário | Priorização | Descrição |
| -------- | ------ | ----------------------------------- |
| US10 | *Must* | Eu, como usuário, desejo ser capaz de visualizar detalhes de uma encomenda |
| US11 | *Could* | Eu, como usuário, desejo ser capaz de visualizar encomendas agrupadas por categorias |
| US12 | *Must* | Eu, como usuário, desejo ser capaz de visualizar histórico de encomendas |
| US13 | *Must* | Eu, como usuário entregador, desejo receber a confirmação do recebimento de uma encomenda |
| US14 | *Must* | Eu, como usuário entregador, desejo visualizar caso a encomenda seja negada |
| US15 | *Must* | Eu, como usuário entregador, desejo visualizar que a caixa está cheia |

### F04 - Notificação

| História de Usuário | Priorização | Descrição |
| -------- | ------ | ----------------------------------- |
| US16 | *Should* | Eu, como usuário, desejo ser capaz de receber uma notificação quando uma encomenda for entregue |
| US17 | *Should* | Eu, como usuário, desejo ser capaz de receber uma notificação quando uma encomenda for negada |
| US18 | *Should* | Eu, como usuário, desejo ser capaz de controlar quais notificações irei receber |
| US19 | *Should* | Eu, como usuário, desejo ser capaz de receber uma notificação quando a caixa estiver cheia |

## E03 - Caixa
### F05 - Sistema Embarcado

| História de Usuário | Priorização | Descrição |
| -------- | ------ | ----------------------------------- |
| US20 | *Must* | Eu, como usuário, desejo que a caixa seja capaz de autorizar abertura da porta |
| US21 | *Must* | Eu, como usuário, desejo que a caixa seja capaz de negar abertura da porta caso esteja cheia |
| US22 | *Must* | Eu, como usuário, desejo que a caixa seja capaz de negar abertura da porta caso o motor linear esteja atuando |
| US23 | *Must* | Eu, como usuário, desejo que a caixa seja capaz de confirmar o recebimento de uma encomenda |
